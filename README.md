# Remote pilot wire

![PCB en 3D](images/pcb_3D.png)

Introduction
=====
Un de mes radiateurs a été recâblé sans son fil pilote, et je me suis demandé comment je pouvais lui transmettre l'information depuis le thermostat existant.

Au fur et à mesure de la réflexion, je me suis dit :

"Pourquoi ne pas faire un thermostat complet, multizones supportant plusieurs consignes par zones ?"

La partie hardware est inspirée de :
https://www.fraifrai.net/index.php?pages/Mon-projet-domotique-libre

Donc, un grand merci à Alex G. et Fraifrai.

Description
=====
Il est composé de deux parties principales :
- La partie hardware qui pilote le(s) chauffage(s) via son(leur) fil pilote.
- La partie thermostat proprement dite (sur une raspberry pi par exemple.)

Schématiquement, ça donne :
```
---------------         ---------------         ---------------              --------------
|             |  MQTT   |             |  MQTT   |             | fils pilotes |            |
| Rpi/ IHM Qt |---------| Mosquitto   |---------|  ESP12F     |--------------| Radiateurs |
|             |         |             |         |             |              |            |
---------------         ---------------         ---------------              --------------
      |
      | Bluetooth Le 4.1
      |
-------------------------------------
|                                   |
| Thermomètres/Hygromètres (Xiaomi) |
|                                   |
-------------------------------------
```
Il faut mettre un thermomètre par zone que l'on veut piloter.

J'ai choisi des thermomètres chez xiaomi (https://maison-de-geek.com/2018/01/test-lhygrothermometre-de-chez-xiaomi/), mais on peut choisir un peu ce que l'on veut, pourvu que l'on puisse récupérer la température via MQTT.

Sur la raspberry Pi, il y a un service qui va jouer le rôle de passerelle bluetooth vers MQTT pour les thermomètres.

J'ai choisi d'installer https://github.com/algirdasc/xiaomi-ble-mqtt

Et de le lancer dans une crontab, toutes les 5 minutes.


Ensuite un logiciel avec une jolie interface sur la Rpi permet de configurer la température dans chaque zone, les plages horaires etc.


Enfin, l'ESP12F est à l'écoute des ordre d'allumage et d'extinction des radiateurs sur un topic MQTT.


Pilotage du radiateur
=====

Pour piloter un radiateur, il faut lui envoyer des ordres via son fil pilote.

Les ordres de bases sont :
- 0V => mode "confort"
- 220V alternatif => mode "éco" (généralement, c'est le mode confort - 3°C)
- 220V alternatif avec les seules alternances négatives : hors gel
- 220V alternatif avec les seules alternances positives : arrêt
Il y a 2 autres modes qui correspondent à confort - 1°C et - 2°C, mais elles sont rarement implémentées.

Pour ce projet, comme nous voulons faire un thermostat, nous aurons simplement besoin de confort et arrêt.

Donc, soit 0V, soit 220V+diode.


Pour couper le 220V, nous nous servons d'un MOC3043 piloté par un ESP12F.

Il n'y a pas besoin de passer par un transistor supplémentaire car l'ESP12F peut fournir assez de courant sur une GPIO pour piloter ce MOC.


L'ordre de marche ou arrêt est envoyé à l'ESP12F via MQTT.

Le PCB est fourni dans kicad/schematics/heating_control

Il permet de piloter 4 chauffages, mais il peut être facilement étendu à 5, voire 7. (Tant qu'il y a des GPIOs de disponibles).

BOM
=====
```
Ref Farnell      Désignation                                         Ref constructeur
-------------------------------------------------------------------------------------
2517749          VTX-214-002-103 ALIMENTATION, AC-DC, 3.3V, 0.6A     VTX-214-002-103
2315560          B32922C3104K189 CONDENSATEUR, 0.1µF, 10%, PP        B32922C3104K189
1360810          0034.3117 FUSIBLE EN VERRE TEMPORISE 1A             0034.3117
2675015          1N4004+ DIODE SIMPLE 400V, 1A, DO-204AL             1N4004+
1021367          MOC3023-M OPTOCOUPLEUR DRIVER DE TRIAC              MOC3023-M
2472316          CBRS01SWH COFFRET POUR CAPTEUR SOLIDE, BLANC        CBRS01SWH
3041499          1729160 BORNIER SUR CI 6V 5.08MM                    1729160
2250941          Varistance TVS, MOV, 275 V, 350 V, 710 V, D14mm     MOV-14D431KTR
```
Les résistances / condensateurs sont assez classiques, le porte fusible et le bouton sont de la récup'

L'ESP-12F se trouve facilement pour quelques euros chez aliexpress ou autre.

Pour la fabrication des PCBs, je l'ai faite chez dirty-PCB, [en prenant quelques notes](docs/HOWTO-gerber-files-for-dirtyPCB.txt)

Logiciel ESP-12F
=====
C'est un "croquis" Arduino.

La version "kitchen" ne pilote qu'un seul chauffage, alors que la version main_pilot_wire_control en contrôle 4.


Le paramétrage WiFi et MQTT se fait dans l'entête du fichier.

TODO: faire un paramétrage via une interface web

```
EspMQTTClient client(
  "rico2", // SSID
  "xxxxx", // Wifi passphrase
  "192.168.1.2",  // MQTT Broker server ip
  "",   // MQTT username (Can be omitted if not needed)
  "",   // MQTT password (Can be omitted if not needed)
  "pilote-general",     // Client name that uniquely identify your device
  1883              // The MQTT port, default to 1883. this line can be omitted
);
```

Les principaux paramètres Arduino sont :
- Type de carte: Generic ESP8266 module
- Upload speed: 921600
- CPU frequency: 80MHz
- Crystal frequency: 26MHz
- Flash Size: 1MB FS none OTA 502KB

Pour flasher la carte, il faut mettre un cavalier sur les broches "Flash"

Thermostat
=====
L'applicatif est fait sous Qt5, pour la raspberry PI avec l'écran tactile 3,5 pouces.

Voici la [procédure d'installation](soft/thermostat/INSTALL)
