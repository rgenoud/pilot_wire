EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Fuse F1
U 1 1 5C48566F
P 2850 6700
F 0 "F1" V 3047 6700 50  0000 C CNN
F 1 "Fuse" V 2956 6700 50  0000 C CNN
F 2 "Fuse:Fuseholder_Cylinder-5x20mm_Schurter_0031_8201_Horizontal_Open" V 2780 6700 50  0001 C CNN
F 3 "~" H 2850 6700 50  0001 C CNN
	1    2850 6700
	-1   0    0    1   
$EndComp
$Comp
L Device:Varistor 14D431
U 1 1 5C485739
P 2700 6350
F 0 "14D431" H 2803 6396 50  0000 L CNN
F 1 "Varistor" H 2803 6305 50  0000 L CNN
F 2 "Varistor:RV_Disc_D15.5mm_W4.2mm_P7.5mm" V 2630 6350 50  0001 C CNN
F 3 "~" H 2700 6350 50  0001 C CNN
	1    2700 6350
	0    -1   -1   0   
$EndComp
$Comp
L Device:C B32922C3104K189
U 1 1 5C4857ED
P 2700 5750
F 0 "B32922C3104K189" H 2815 5796 50  0001 L CNN
F 1 "C" H 2815 5705 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L18.0mm_W5.0mm_P15.00mm_FKS3_FKP3" H 2738 5600 50  0001 C CNN
F 3 "~" H 2700 5750 50  0001 C CNN
	1    2700 5750
	0    -1   -1   0   
$EndComp
$Comp
L Device:CP C1
U 1 1 5C485FAB
P 2700 4500
F 0 "C1" H 2818 4546 50  0000 L CNN
F 1 "47µF" H 2818 4455 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D6.3mm_P2.50mm" H 2738 4350 50  0001 C CNN
F 3 "~" H 2700 4500 50  0001 C CNN
	1    2700 4500
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR0101
U 1 1 5C48610F
P 2300 4500
F 0 "#PWR0101" H 2300 4350 50  0001 C CNN
F 1 "+3.3V" H 2315 4673 50  0000 C CNN
F 2 "" H 2300 4500 50  0001 C CNN
F 3 "" H 2300 4500 50  0001 C CNN
	1    2300 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0102
U 1 1 5C4863DE
P 3150 4600
F 0 "#PWR0102" H 3150 4400 50  0001 C CNN
F 1 "GNDPWR" H 3154 4446 50  0001 C CNN
F 2 "" H 3150 4550 50  0001 C CNN
F 3 "" H 3150 4550 50  0001 C CNN
	1    3150 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:D D11
U 1 1 5C4869B6
P 5050 6550
F 0 "D11" H 5050 6766 50  0001 C CNN
F 1 "D" H 5050 6674 50  0001 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 5050 6550 50  0001 C CNN
F 3 "~" H 5050 6550 50  0001 C CNN
	1    5050 6550
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D12
U 1 1 5C486A91
P 5250 6550
F 0 "D12" H 5250 6766 50  0001 C CNN
F 1 "D" H 5250 6674 50  0001 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 5250 6550 50  0001 C CNN
F 3 "~" H 5250 6550 50  0001 C CNN
	1    5250 6550
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D13
U 1 1 5C486AAA
P 5450 6550
F 0 "D13" H 5450 6766 50  0001 C CNN
F 1 "D" H 5450 6674 50  0001 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 5450 6550 50  0001 C CNN
F 3 "~" H 5450 6550 50  0001 C CNN
	1    5450 6550
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D14
U 1 1 5C486AC3
P 5650 6550
F 0 "D14" H 5650 6766 50  0001 C CNN
F 1 "D" H 5650 6674 50  0001 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 5650 6550 50  0001 C CNN
F 3 "~" H 5650 6550 50  0001 C CNN
	1    5650 6550
	0    -1   -1   0   
$EndComp
$Comp
L heating_control-rescue:MOC3043M-Relay_SolidState U14
U 1 1 5C489E04
P 7750 4200
F 0 "U14" V 7704 4388 50  0000 L CNN
F 1 "MOC3043M" V 7795 4388 50  0000 L CNN
F 2 "Package_DIP:DIP-6_W7.62mm" H 7550 4000 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/MO/MOC3031M.pdf" H 7715 4200 50  0001 L CNN
	1    7750 4200
	0    1    1    0   
$EndComp
$Comp
L heating_control-rescue:MOC3043M-Relay_SolidState U13
U 1 1 5C48AF61
P 6800 4200
F 0 "U13" V 6754 4388 50  0000 L CNN
F 1 "MOC3043M" V 6845 4388 50  0000 L CNN
F 2 "Package_DIP:DIP-6_W7.62mm" H 6600 4000 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/MO/MOC3031M.pdf" H 6765 4200 50  0001 L CNN
	1    6800 4200
	0    1    1    0   
$EndComp
$Comp
L heating_control-rescue:MOC3043M-Relay_SolidState U12
U 1 1 5C48FEC8
P 5850 4200
F 0 "U12" V 5804 4388 50  0000 L CNN
F 1 "MOC3043M" V 5895 4388 50  0000 L CNN
F 2 "Package_DIP:DIP-6_W7.62mm" H 5650 4000 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/MO/MOC3031M.pdf" H 5815 4200 50  0001 L CNN
	1    5850 4200
	0    1    1    0   
$EndComp
$Comp
L heating_control-rescue:MOC3043M-Relay_SolidState U11
U 1 1 5C491810
P 5000 4200
F 0 "U11" V 4954 4388 50  0000 L CNN
F 1 "MOC3043M" V 5045 4388 50  0000 L CNN
F 2 "Package_DIP:DIP-6_W7.62mm" H 4800 4000 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/MO/MOC3031M.pdf" H 4965 4200 50  0001 L CNN
	1    5000 4200
	0    1    1    0   
$EndComp
$Comp
L RF_Module:ESP-12F U2
U 1 1 5C494524
P 3550 2850
F 0 "U2" H 4000 2200 50  0000 C CNN
F 1 "ESP-12F" H 3850 3600 50  0000 C CNN
F 2 "RF_Module:ESP-12E" H 3550 2850 50  0001 C CNN
F 3 "http://wiki.ai-thinker.com/_media/esp8266/esp8266_series_modules_user_manual_v1.1.pdf" H 3200 2950 50  0001 C CNN
	1    3550 2850
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0103
U 1 1 5C494719
P 3550 1300
F 0 "#PWR0103" H 3550 1150 50  0001 C CNN
F 1 "+3.3V" H 3565 1473 50  0000 C CNN
F 2 "" H 3550 1300 50  0001 C CNN
F 3 "" H 3550 1300 50  0001 C CNN
	1    3550 1300
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0104
U 1 1 5C4967A1
P 2450 2350
F 0 "#PWR0104" H 2450 2200 50  0001 C CNN
F 1 "+3.3V" H 2465 2523 50  0000 C CNN
F 2 "" H 2450 2350 50  0001 C CNN
F 3 "" H 2450 2350 50  0001 C CNN
	1    2450 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5C49685A
P 2700 2450
F 0 "R1" V 2493 2450 50  0000 C CNN
F 1 "10K" V 2584 2450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2630 2450 50  0001 C CNN
F 3 "~" H 2700 2450 50  0001 C CNN
	1    2700 2450
	0    1    1    0   
$EndComp
Wire Wire Line
	2950 2450 2850 2450
Wire Wire Line
	2550 2450 2450 2450
Wire Wire Line
	2450 2450 2450 2350
$Comp
L Device:R R14
U 1 1 5C49ED32
P 7850 3550
F 0 "R14" V 7643 3550 50  0000 C CNN
F 1 "330" V 7734 3550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7780 3550 50  0001 C CNN
F 3 "~" H 7850 3550 50  0001 C CNN
	1    7850 3550
	-1   0    0    1   
$EndComp
$Comp
L Device:R R13
U 1 1 5C49ED7A
P 6900 3500
F 0 "R13" V 6693 3500 50  0000 C CNN
F 1 "330" V 6784 3500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6830 3500 50  0001 C CNN
F 3 "~" H 6900 3500 50  0001 C CNN
	1    6900 3500
	-1   0    0    1   
$EndComp
$Comp
L Device:R R12
U 1 1 5C49EDC6
P 5950 3500
F 0 "R12" V 5743 3500 50  0000 C CNN
F 1 "330" V 5834 3500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5880 3500 50  0001 C CNN
F 3 "~" H 5950 3500 50  0001 C CNN
	1    5950 3500
	-1   0    0    1   
$EndComp
$Comp
L Device:R R11
U 1 1 5C49EE16
P 5100 3500
F 0 "R11" V 4893 3500 50  0000 C CNN
F 1 "330" V 4984 3500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5030 3500 50  0001 C CNN
F 3 "~" H 5100 3500 50  0001 C CNN
	1    5100 3500
	-1   0    0    1   
$EndComp
$Comp
L power:GNDPWR #PWR0106
U 1 1 5C49EF1F
P 7400 3900
F 0 "#PWR0106" H 7400 3700 50  0001 C CNN
F 1 "GNDPWR" H 7404 3746 50  0001 C CNN
F 2 "" H 7400 3850 50  0001 C CNN
F 3 "" H 7400 3850 50  0001 C CNN
	1    7400 3900
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0107
U 1 1 5C49EF76
P 6500 3900
F 0 "#PWR0107" H 6500 3700 50  0001 C CNN
F 1 "GNDPWR" H 6504 3746 50  0001 C CNN
F 2 "" H 6500 3850 50  0001 C CNN
F 3 "" H 6500 3850 50  0001 C CNN
	1    6500 3900
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0108
U 1 1 5C49EFA7
P 5550 3900
F 0 "#PWR0108" H 5550 3700 50  0001 C CNN
F 1 "GNDPWR" H 5554 3746 50  0001 C CNN
F 2 "" H 5550 3850 50  0001 C CNN
F 3 "" H 5550 3850 50  0001 C CNN
	1    5550 3900
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0109
U 1 1 5C49EFD8
P 4650 3900
F 0 "#PWR0109" H 4650 3700 50  0001 C CNN
F 1 "GNDPWR" H 4654 3746 50  0001 C CNN
F 2 "" H 4650 3850 50  0001 C CNN
F 3 "" H 4650 3850 50  0001 C CNN
	1    4650 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 3550 3550 3600
$Comp
L power:+3.3V #PWR0110
U 1 1 5C4BB49F
P 4150 1750
F 0 "#PWR0110" H 4150 1600 50  0001 C CNN
F 1 "+3.3V" H 4165 1923 50  0000 C CNN
F 2 "" H 4150 1750 50  0001 C CNN
F 3 "" H 4150 1750 50  0001 C CNN
	1    4150 1750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5C4BB501
P 4150 1950
F 0 "R2" H 4220 1996 50  0000 L CNN
F 1 "10K" H 4220 1905 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4080 1950 50  0001 C CNN
F 3 "~" H 4150 1950 50  0001 C CNN
	1    4150 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NO_Small JP1
U 1 1 5C4BFD5B
P 4500 2100
F 0 "JP1" H 4500 2193 50  0000 C CNN
F 1 "Jumper_NO_Small" H 4500 2194 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4500 2100 50  0001 C CNN
F 3 "~" H 4500 2100 50  0001 C CNN
	1    4500 2100
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0111
U 1 1 5C4BFE37
P 4700 2100
F 0 "#PWR0111" H 4700 1900 50  0001 C CNN
F 1 "GNDPWR" H 4704 1946 50  0001 C CNN
F 2 "" H 4700 2050 50  0001 C CNN
F 3 "" H 4700 2050 50  0001 C CNN
	1    4700 2100
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0112
U 1 1 5C4C4836
P 3550 3600
F 0 "#PWR0112" H 3550 3400 50  0001 C CNN
F 1 "GNDPWR" H 3554 3446 50  0001 C CNN
F 2 "" H 3550 3550 50  0001 C CNN
F 3 "" H 3550 3550 50  0001 C CNN
	1    3550 3600
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0113
U 1 1 5C4CDF44
P 4350 3600
F 0 "#PWR0113" H 4350 3400 50  0001 C CNN
F 1 "GNDPWR" H 4354 3446 50  0001 C CNN
F 2 "" H 4350 3550 50  0001 C CNN
F 3 "" H 4350 3550 50  0001 C CNN
	1    4350 3600
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0114
U 1 1 5C4D05DE
P 4900 1750
F 0 "#PWR0114" H 4900 1600 50  0001 C CNN
F 1 "+3.3V" H 4915 1923 50  0000 C CNN
F 2 "" H 4900 1750 50  0001 C CNN
F 3 "" H 4900 1750 50  0001 C CNN
	1    4900 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 1800 4150 1750
Wire Wire Line
	4150 2250 4150 2100
Connection ~ 4150 2100
$Comp
L power:GNDPWR #PWR0115
U 1 1 5C4DEE2C
P 5950 1700
F 0 "#PWR0115" H 5950 1500 50  0001 C CNN
F 1 "GNDPWR" H 5954 1546 50  0001 C CNN
F 2 "" H 5950 1650 50  0001 C CNN
F 3 "" H 5950 1650 50  0001 C CNN
	1    5950 1700
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0116
U 1 1 5C4DEECA
P 6500 2250
F 0 "#PWR0116" H 6500 2100 50  0001 C CNN
F 1 "+3.3V" H 6515 2423 50  0000 C CNN
F 2 "" H 6500 2250 50  0001 C CNN
F 3 "" H 6500 2250 50  0001 C CNN
	1    6500 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 2400 6600 2400
Wire Wire Line
	4150 2350 4250 2350
Wire Wire Line
	4250 2350 4250 2300
Wire Wire Line
	4250 2300 5350 2300
Wire Wire Line
	6000 1650 5950 1650
Wire Wire Line
	5950 1650 5950 1700
Wire Wire Line
	6500 2250 6500 2400
Wire Wire Line
	5150 2550 5150 2400
Wire Wire Line
	5150 2400 5350 2400
Wire Wire Line
	4150 2550 5150 2550
$Comp
L Device:R R3
U 1 1 5C4F0EED
P 4900 1950
F 0 "R3" H 4970 1996 50  0000 L CNN
F 1 "10K" H 4970 1905 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4830 1950 50  0001 C CNN
F 3 "~" H 4900 1950 50  0001 C CNN
	1    4900 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 2450 4900 2100
Wire Wire Line
	4150 2450 4900 2450
Wire Wire Line
	4900 1750 4900 1800
$Comp
L Device:R R4
U 1 1 5C4F78AC
P 4350 3400
F 0 "R4" H 4420 3446 50  0000 L CNN
F 1 "10K" H 4420 3355 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4280 3400 50  0001 C CNN
F 3 "~" H 4350 3400 50  0001 C CNN
	1    4350 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 3600 4350 3550
Wire Wire Line
	4150 3150 4350 3150
Wire Wire Line
	4350 3150 4350 3250
NoConn ~ 2950 3350
NoConn ~ 2950 3250
NoConn ~ 2950 3150
NoConn ~ 2950 3050
NoConn ~ 2950 2950
NoConn ~ 2950 2850
NoConn ~ 2950 2650
Wire Wire Line
	4700 2100 4600 2100
Wire Wire Line
	4150 2100 4400 2100
$Comp
L Device:R R5
U 1 1 5C48BDB5
P 2950 1900
F 0 "R5" H 3020 1946 50  0000 L CNN
F 1 "10K" H 3020 1855 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2880 1900 50  0001 C CNN
F 3 "~" H 2950 1900 50  0001 C CNN
	1    2950 1900
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0117
U 1 1 5C48BE31
P 2950 1650
F 0 "#PWR0117" H 2950 1500 50  0001 C CNN
F 1 "+3.3V" H 2965 1823 50  0000 C CNN
F 2 "" H 2950 1650 50  0001 C CNN
F 3 "" H 2950 1650 50  0001 C CNN
	1    2950 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 2250 2950 2100
Wire Wire Line
	2950 1750 2950 1650
$Comp
L Switch:SW_Push SW1
U 1 1 5C492B38
P 2050 2100
F 0 "SW1" H 2050 2385 50  0000 C CNN
F 1 "SW_Push" H 2050 2294 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_TL3305A" H 2050 2300 50  0001 C CNN
F 3 "" H 2050 2300 50  0001 C CNN
	1    2050 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5C492C6E
P 2100 2400
F 0 "C3" V 1848 2400 50  0000 C CNN
F 1 "100n" V 1939 2400 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 2138 2250 50  0001 C CNN
F 3 "~" H 2100 2400 50  0001 C CNN
	1    2100 2400
	0    1    1    0   
$EndComp
Wire Wire Line
	2950 2100 2300 2100
Connection ~ 2950 2100
Wire Wire Line
	2950 2100 2950 2050
$Comp
L power:GNDPWR #PWR0118
U 1 1 5C499A38
P 1850 2450
F 0 "#PWR0118" H 1850 2250 50  0001 C CNN
F 1 "GNDPWR" H 1854 2296 50  0001 C CNN
F 2 "" H 1850 2400 50  0001 C CNN
F 3 "" H 1850 2400 50  0001 C CNN
	1    1850 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 2400 2300 2100
Connection ~ 2300 2100
Wire Wire Line
	2300 2100 2250 2100
Wire Wire Line
	2300 2400 2250 2400
Wire Wire Line
	1850 2100 1850 2400
Wire Wire Line
	1950 2400 1850 2400
Connection ~ 1850 2400
Wire Wire Line
	1850 2400 1850 2450
$Comp
L Device:Jumper_NO_Small JP2
U 1 1 5C4ACA98
P 3300 3750
F 0 "JP2" H 3300 3843 50  0000 C CNN
F 1 "Jumper_NO_Small" H 3300 3844 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 3300 3750 50  0001 C CNN
F 3 "~" H 3300 3750 50  0001 C CNN
	1    3300 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 3050 5100 3050
Wire Wire Line
	6900 2950 6900 3350
Wire Wire Line
	4150 2950 6900 2950
Wire Wire Line
	7850 2650 7850 3400
$Comp
L vtx-214-002-103:VTX-214-002-103 PS1
U 1 1 5C4F83D7
P 2750 5100
F 0 "PS1" V 2796 4870 50  0000 R CNN
F 1 "VTX-214-002-103" V 2705 4870 50  0000 R CNN
F 2 "Converter_ACDC:Converter_ACDC_Vigortronix_Low_Profile_VTX-214-002-1xx_THT" H 2750 5550 50  0001 C CNN
F 3 "http://www.vigortronix.com/2WLowProfileAC-DCConverters" H 3850 4300 50  0001 C CNN
	1    2750 5100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4150 3250 4150 3750
Wire Wire Line
	4150 3750 3400 3750
Wire Wire Line
	3200 3750 2300 3750
Wire Wire Line
	2300 3750 2300 2400
Connection ~ 2300 2400
Wire Wire Line
	2850 4700 2850 4500
Connection ~ 2850 4500
Wire Wire Line
	2850 4500 3150 4500
Wire Wire Line
	3150 4500 3150 4600
Wire Wire Line
	2650 4700 2550 4700
Wire Wire Line
	2550 4700 2550 4500
Connection ~ 2550 4500
Wire Wire Line
	2550 4500 2300 4500
Wire Wire Line
	2850 5500 2850 5750
Connection ~ 2850 5750
Wire Wire Line
	2850 6350 2850 6550
Connection ~ 2850 6350
Wire Wire Line
	2650 5500 2550 5500
Wire Wire Line
	2550 5500 2550 5750
Wire Wire Line
	2550 5750 2550 6350
Connection ~ 2550 5750
Wire Wire Line
	5050 6700 5050 7100
Wire Wire Line
	5250 6700 5250 6850
Wire Wire Line
	5250 6850 5150 6850
Wire Wire Line
	5150 6850 5150 7100
Wire Wire Line
	5450 6700 5450 6900
Wire Wire Line
	5450 6900 5250 6900
Wire Wire Line
	5250 6900 5250 7100
Wire Wire Line
	5650 6700 5650 6950
Wire Wire Line
	5650 6950 5350 6950
Wire Wire Line
	5350 6950 5350 7100
Wire Wire Line
	4900 4500 4900 6400
Wire Wire Line
	4900 6400 5050 6400
Wire Wire Line
	2850 5750 2850 6350
Connection ~ 2850 5500
Wire Wire Line
	7650 4500 7650 5500
Wire Wire Line
	6900 4500 6900 5500
Connection ~ 6900 5500
Wire Wire Line
	6900 5500 7650 5500
Wire Wire Line
	5750 4500 5750 5500
Connection ~ 5750 5500
Wire Wire Line
	5750 5500 6900 5500
Wire Wire Line
	5100 4500 5100 5500
Connection ~ 5100 5500
Wire Wire Line
	5100 5500 5750 5500
Wire Wire Line
	5950 4500 5950 5800
Wire Wire Line
	5950 5800 5250 5800
Wire Wire Line
	5250 5800 5250 6400
Wire Wire Line
	6700 4500 6700 5900
Wire Wire Line
	6700 5900 5450 5900
Wire Wire Line
	5450 5900 5450 6400
Wire Wire Line
	7850 4500 7850 6000
Wire Wire Line
	7850 6000 5650 6000
Wire Wire Line
	5650 6000 5650 6400
Wire Wire Line
	5950 3650 5950 3900
Wire Wire Line
	6900 3650 6900 3900
Wire Wire Line
	7850 3700 7850 3900
Wire Wire Line
	7650 3900 7400 3900
Wire Wire Line
	6700 3900 6500 3900
Wire Wire Line
	5750 3900 5550 3900
Wire Wire Line
	4900 3900 4650 3900
Wire Wire Line
	5100 3050 5100 3350
Wire Wire Line
	5100 3650 5100 3900
Wire Wire Line
	4150 2850 5950 2850
Wire Wire Line
	5950 2850 5950 3350
Wire Wire Line
	4150 2650 7850 2650
$Comp
L Connector:Conn_01x01_Male J2
U 1 1 5C54EBB1
P 6800 2400
F 0 "J2" H 6773 2330 50  0000 R CNN
F 1 "Conn_01x01_Male" H 6773 2421 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 6800 2400 50  0001 C CNN
F 3 "~" H 6800 2400 50  0001 C CNN
	1    6800 2400
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Male J3
U 1 1 5C55516F
P 6200 1650
F 0 "J3" H 6173 1580 50  0000 R CNN
F 1 "Conn_01x01_Male" H 6173 1671 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 6200 1650 50  0001 C CNN
F 3 "~" H 6200 1650 50  0001 C CNN
	1    6200 1650
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Male J4
U 1 1 5C555245
P 5550 2400
F 0 "J4" H 5523 2280 50  0000 R CNN
F 1 "Conn_01x02_Male" H 5523 2371 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5550 2400 50  0001 C CNN
F 3 "~" H 5550 2400 50  0001 C CNN
	1    5550 2400
	-1   0    0    1   
$EndComp
$Comp
L Connector:Screw_Terminal_01x06 J1
U 1 1 5D154ECA
P 5150 7300
F 0 "J1" V 5023 7580 50  0000 L CNN
F 1 "Screw_Terminal_01x06" V 5114 7580 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-6-5.08_1x06_P5.08mm_Horizontal" H 5150 7300 50  0001 C CNN
F 3 "~" H 5150 7300 50  0001 C CNN
	1    5150 7300
	0    1    1    0   
$EndComp
Wire Wire Line
	2850 6850 2850 7100
Wire Wire Line
	2550 6350 2550 7000
Wire Wire Line
	4950 7000 4950 7100
Connection ~ 2550 6350
$Comp
L Device:C C4
U 1 1 5D405135
P 3700 1600
F 0 "C4" V 3448 1600 50  0000 C CNN
F 1 "100n" V 3539 1600 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 3738 1450 50  0001 C CNN
F 3 "~" H 3700 1600 50  0001 C CNN
	1    3700 1600
	0    -1   -1   0   
$EndComp
NoConn ~ 4150 2750
Wire Wire Line
	2850 7100 4850 7100
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5D4168EF
P 4150 7000
F 0 "#FLG0101" H 4150 7075 50  0001 C CNN
F 1 "PWR_FLAG" H 4150 7174 50  0000 C CNN
F 2 "" H 4150 7000 50  0001 C CNN
F 3 "~" H 4150 7000 50  0001 C CNN
	1    4150 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 1300 3550 1600
Connection ~ 3550 1600
$Comp
L power:GNDPWR #PWR0105
U 1 1 5D43678D
P 3900 1600
F 0 "#PWR0105" H 3900 1400 50  0001 C CNN
F 1 "GNDPWR" H 3904 1446 50  0001 C CNN
F 2 "" H 3900 1550 50  0001 C CNN
F 3 "" H 3900 1550 50  0001 C CNN
	1    3900 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 1600 3900 1600
Wire Wire Line
	3550 1600 3550 2050
Connection ~ 4150 7000
Wire Wire Line
	2550 7000 4150 7000
Wire Wire Line
	4150 7000 4950 7000
Wire Wire Line
	4200 5500 5100 5500
Wire Wire Line
	2850 5500 4200 5500
Connection ~ 4200 5500
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5D43D51D
P 4200 5500
F 0 "#FLG0102" H 4200 5575 50  0001 C CNN
F 1 "PWR_FLAG" H 4200 5674 50  0000 C CNN
F 2 "" H 4200 5500 50  0001 C CNN
F 3 "~" H 4200 5500 50  0001 C CNN
	1    4200 5500
	1    0    0    -1  
$EndComp
$EndSCHEMATC
