QT += core gui network

QT += mqtt

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = thermostat
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += src/main.cpp \
	src/mainwindow.cpp \
	src/zoneitem.cpp \
	src/mqttclient.cpp \
	src/settings.cpp \
	src/boost_dlg.cpp \
	src/menu_dlg.cpp \
	src/holiday_dlg.cpp \
	src/edit_dlg.cpp \
	src/edit_prog_dlg.cpp \


HEADERS += inc/mainwindow.h \
	inc/zoneitem.h \
	inc/mqttclient.h \
	inc/settings.h \
	inc/boost_dlg.h \
	inc/menu_dlg.h \
	inc/holiday_dlg.h \
	inc/edit_dlg.h \
	inc/edit_prog_dlg.h \

RESOURCES += thermostat.qrc

TRANSLATION_DIR = lang

TRANSLATIONS += $$TRANSLATION_DIR/thermostat_fr.ts

# This is not working with Qt5.7 on debian.
CONFIG += lrelease embed_translations

target.path = $$[INSTALL_ROOT]/usr/bin/
INSTALLS += target

VERSION = $$system(./scripts/get_tag_version.sh)

# to see debug messages in console, run with environment variable QT_LOGGING_RULES="debug=true;"
QMAKE_CXXFLAGS += -Wall -Werror
QMAKE_CXXFLAGS += -std=c++11
QMAKE_CXXFLAGS += -DVERSION=\'\"$$VERSION\"\'
QMAKE_CXXFLAGS_DEBUG += -O0
QMAKE_CXXFLAGS_RELEASE += -Os

unix: CONFIG += link_pkgconfig

INCLUDEPATH += inc/ \

LUPDATE = $$shell_path($$[QT_HOST_BINS]/lupdate) -locations relative -no-ui-lines -no-sort
LRELEASE = $$shell_path($$[QT_HOST_BINS]/lrelease) -compress -nounfinished -removeidentical
LCONVERT = $$shell_path($$[QT_HOST_BINS]/lconvert)
langupd.command = $$LUPDATE $$shell_path($$_PRO_FILE_) -ts $$_PRO_FILE_PWD_/$$TRANSLATIONS

langrel.depends = langupd
langrel.input = TRANSLATIONS
langrel.output = $$TRANSLATION_DIR/${QMAKE_FILE_BASE}.qm
langrel.commands = $$LRELEASE ${QMAKE_FILE_IN}
langrel.CONFIG += no_link

QMAKE_EXTRA_TARGETS += langupd
QMAKE_EXTRA_COMPILERS += langrel
PRE_TARGETDEPS += langupd compiler_langrel_make_all
