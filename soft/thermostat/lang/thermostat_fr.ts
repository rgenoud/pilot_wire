<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>BoostDlg</name>
    <message>
        <source>Temporarily set %1 temperature: (actual target: %2 °C)</source>
        <translation type="vanished">Fixe temporairement la T°C
de %1 (consigne actuelle %2 °C)</translation>
    </message>
    <message>
        <location filename="../src/boost_dlg.cpp" line="+32"/>
        <source>Temporarily set %1 temperature (actual: %2 °C / target: %3 °C)</source>
        <translation>Fixe temporairement la T°C
de %1 (actuelle: %2 °C / consigne %3 °C)</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>EditDlg</name>
    <message>
        <location filename="../src/edit_dlg.cpp" line="+39"/>
        <source>%1 default target:</source>
        <translation>Consigne par défaut
de %1 :</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location line="-1"/>
        <source>Add</source>
        <translation>+</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location line="+160"/>
        <source>M</source>
        <translation>L</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>W</source>
        <translation>Me</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>F</source>
        <translation>V</translation>
    </message>
    <message>
        <source>Mo</source>
        <translation type="vanished">Lu</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>Tu</source>
        <translation>Ma</translation>
    </message>
    <message>
        <source>We</source>
        <translation type="vanished">Me</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Th</source>
        <translation>J</translation>
    </message>
    <message>
        <source>Fr</source>
        <translation type="vanished">Ve</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sa</source>
        <translation>S</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Su</source>
        <translation>D</translation>
    </message>
    <message>
        <location line="-97"/>
        <source>Edit</source>
        <translation>Modif</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete</source>
        <translation>Suppr</translation>
    </message>
</context>
<context>
    <name>EditProgDlg</name>
    <message>
        <location filename="../src/edit_prog_dlg.cpp" line="+35"/>
        <source>target:</source>
        <translation>consigne :</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Mo</source>
        <translation>Lu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tu</source>
        <translation>Ma</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>We</source>
        <translation>Me</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Th</source>
        <translation>Je</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Fr</source>
        <translation>Ve</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sa</source>
        <translation>Sa</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Su</source>
        <translation>Di</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Start time:</source>
        <translation>Début :</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>End time:</source>
        <translation>Fin :</translation>
    </message>
</context>
<context>
    <name>HolidayDlg</name>
    <message>
        <location filename="../src/holiday_dlg.cpp" line="+38"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="+58"/>
        <source>Menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Sorico&apos;s thermostat</source>
        <translation>Thermostat de So et Rico</translation>
    </message>
    <message>
        <location line="+198"/>
        <source>Force off</source>
        <translation>Chauffages
Éteints</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Force on</source>
        <translation>Chauffages
en marche
forcée</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location line="+157"/>
        <source>Holiday mode until %1</source>
        <translation>Mode vacances jusqu&apos;au %1</translation>
    </message>
</context>
<context>
    <name>MenuDlg</name>
    <message>
        <location filename="../src/menu_dlg.cpp" line="+27"/>
        <source>Holidays</source>
        <translation>Vacances</translation>
    </message>
    <message>
        <source>Save configuration</source>
        <translation type="vanished">Enregistrer la 
configuration</translation>
    </message>
    <message>
        <source>Load configuration</source>
        <translation type="vanished">Charger la
configuration</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Exit Application</source>
        <translation>Quitter
l&apos;application</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Back</source>
        <translation>Retour</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Exit application</source>
        <translation>Quitter l&apos;application</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Are you sure ?</source>
        <translation>Êtes-vous sûr ?</translation>
    </message>
</context>
<context>
    <name>ZoneItem</name>
    <message>
        <source>Living room</source>
        <translation type="vanished">Salon</translation>
    </message>
</context>
</TS>
