// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Qt mutizone MQTT thermostat
 *
 * Copyright (C) 2020 Richard Genoud
 *
 */

#include <QDialogButtonBox>
#include <QSpinBox>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLocale>
#include <QLabel>
#include <QtMath>
#include <QDateTime>
#include <QTime>

#include "settings.h"
#include "boost_dlg.h"

#define SPIN_ARROW_W 75
#define SPIN_ARROW_H 50
#define SPIN_FONT_SZ 20

BoostDlg::BoostDlg(int idx, const QString &zoneName,
		   double actual_temperature, double target_temperature,
		   QWidget *parent, Qt::WindowFlags f) : QWidget(parent, f)
{
	QString sheet;
	QLabel *room_name = new QLabel(QString(tr("Temporarily set %1 temperature (actual: %2 °C / target: %3 °C)")
					       .arg(zoneName)
					       .arg(actual_temperature)
					       .arg(target_temperature)));

	QFont font = room_name->font();
	font.setPointSize(12);
	font.setBold(true);
	room_name->setFont(font);

	QSizePolicy *szQPolicy = new QSizePolicy(QSizePolicy::Minimum,
						QSizePolicy::MinimumExpanding,
						QSizePolicy::PushButton);
	room_name->setSizePolicy(*szQPolicy);

	actual_temperature = qFloor(actual_temperature * 2) / 2.0;
	m_idx = idx;
	m_boost_temperature.setDecimals(1);
	m_boost_temperature.setValue(actual_temperature);
	m_boost_temperature.setSingleStep(0.5);
	m_boost_temperature.setSuffix(" °C");

	m_boost_temperature.setFont(font);

	/*
	 * TODO: setting size in pixels sucks
	 * We should set size in mm or ratio of the window size
	 */
	sheet = QString("QDoubleSpinBox { height: %1px;  font-size: %4px }"
		"QDoubleSpinBox::up-button { width: %2px; height: %3px }"
		"QDoubleSpinBox::down-button { width: %2px; height: %3px }")
		.arg(2*SPIN_ARROW_H)
		.arg(SPIN_ARROW_W)
		.arg(SPIN_ARROW_H)
		.arg(SPIN_FONT_SZ);
	m_boost_temperature.setStyleSheet(sheet);

	m_boost_duration.setRange(0, 99);
	m_boost_duration.setValue(1);
	m_boost_duration.setSuffix(" H");
	sheet = sheet.replace("QDoubleSpinBox", "QSpinBox");
	m_boost_duration.setStyleSheet(sheet);

	m_boost_duration.setFont(font);

	QHBoxLayout *spinLayout = new QHBoxLayout;
	spinLayout->addWidget(&m_boost_temperature);
	spinLayout->addWidget(&m_boost_duration);

	QPushButton *ok_btn = new QPushButton(tr("Ok"), this);
	QPushButton *cancel_btn = new QPushButton(tr("Cancel"), this);


	QSizePolicy *szPolicy = new QSizePolicy(QSizePolicy::Minimum,
						QSizePolicy::MinimumExpanding,
						QSizePolicy::PushButton);

	ok_btn->setFont(font);
	cancel_btn->setFont(font);

	ok_btn->setSizePolicy(*szPolicy);
	cancel_btn->setSizePolicy(*szPolicy);

	connect(ok_btn, SIGNAL(clicked(void)), this, SLOT(send_result(void)));
	connect(cancel_btn, SIGNAL(clicked(void)), this, SLOT(reject(void)));
	connect(&m_boost_duration, SIGNAL(valueChanged(int)), this, SLOT(updateDuration(int)));

	QHBoxLayout *btnLayout = new QHBoxLayout;

	btnLayout->addWidget(cancel_btn);
	btnLayout->addWidget(ok_btn);

	QVBoxLayout *topLayout = new QVBoxLayout;

	topLayout->addWidget(room_name);
	topLayout->addLayout(spinLayout);
	topLayout->addLayout(btnLayout);

	this->setLayout(topLayout);
}

void BoostDlg::updateDuration(int val)
{
	QTime duration = QTime::currentTime();
	duration = duration.addSecs(val * 3600);

	m_boost_duration.setSuffix(QString(" H (%1)").arg(duration.toString("HH:mm")));
}

void BoostDlg::send_result(void)
{
	struct boost_data data;
	QDateTime now = QDateTime::currentDateTime();

	data.temperature = m_boost_temperature.value();
	data.end_date = now.addSecs(m_boost_duration.value() * 3600);

	emit boost_changed(m_idx, data);
}

void BoostDlg::reject(void)
{
	struct boost_data data;

	emit boost_changed(-1, data);
}

BoostDlg::~BoostDlg()
{
}

/* vim: set tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab: */

