// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Qt mutizone MQTT thermostat
 *
 * Copyright (C) 2020 Richard Genoud
 *
 */

#include <QApplication>
#include <QPushButton>
#include <QCalendarWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QSizePolicy>
#include <QDateTime>
#include <QSpinBox>
#include <QLocale>
#include <QTime>

#include "settings.h"
#include "holiday_dlg.h"

HolidayDlg::HolidayDlg(QWidget *parent, Qt::WindowFlags f) :
	QWidget(parent, f)
{
	QVBoxLayout *middleLayout = new QVBoxLayout;
	QHBoxLayout *mainLayout = new QHBoxLayout;

	m_cal.setGridVisible(true);
	m_cal.setMinimumDate(QDate::currentDate());
	m_cal.setDateEditEnabled(false);
	m_cal.setVerticalHeaderFormat(QCalendarWidget::NoVerticalHeader);
	m_time.setRange(0, 23);
	m_time.setValue(12);
	m_time.setSuffix(" H");
	m_time.setWrapping(true);
	m_time.setReadOnly(true);
	m_time.setButtonSymbols(QAbstractSpinBox::NoButtons);

	QSizePolicy *szPolicy = new QSizePolicy(QSizePolicy::Minimum,
						QSizePolicy::MinimumExpanding,
						QSizePolicy::PushButton);

	QPushButton *next_btn = new QPushButton("H+", this);
	QPushButton *prev_btn = new QPushButton("H-", this);
	QPushButton *ok_btn = new QPushButton(tr("Ok"), this);
	QPushButton *cancel_btn = new QPushButton(tr("Cancel"), this);

	QFont font = next_btn->font();
	font.setPointSize(12);
	font.setBold(true);
	next_btn->setFont(font);
	prev_btn->setFont(font);
	ok_btn->setFont(font);
	cancel_btn->setFont(font);
	m_cal.setFont(font);
	m_time.setFont(font);

	next_btn->setSizePolicy(*szPolicy);
	prev_btn->setSizePolicy(*szPolicy);
	ok_btn->setSizePolicy(*szPolicy);
	cancel_btn->setSizePolicy(*szPolicy);

	middleLayout->addWidget(&m_time);
	middleLayout->addWidget(next_btn);
	middleLayout->addWidget(prev_btn);
	middleLayout->addWidget(ok_btn);
	middleLayout->addWidget(cancel_btn);

	mainLayout->addWidget(&m_cal);
	mainLayout->addLayout(middleLayout);

	connect(next_btn, SIGNAL(clicked(void)), &m_time, SLOT(stepUp(void)));
	connect(prev_btn, SIGNAL(clicked(void)), &m_time, SLOT(stepDown(void)));
	connect(ok_btn, SIGNAL(clicked(void)), this, SLOT(validate_holiday_mode(void)));
	connect(cancel_btn, SIGNAL(clicked(void)), this, SIGNAL(close_holiday_dlg(void)));


	this->setLayout(mainLayout);
}

HolidayDlg::~HolidayDlg()
{
}

void HolidayDlg::validate_holiday_mode(void)
{
	emit holiday_mode(QDateTime(m_cal.selectedDate(), QTime(m_time.value(), 0)));
	emit close_holiday_dlg();
}

/* vim: set tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab: */

