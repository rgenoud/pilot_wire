// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Qt mutizone MQTT thermostat
 *
 * Copyright (C) 2020 Richard Genoud
 *
 */

#include <QLoggingCategory>
#include <QDoubleSpinBox>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLocale>
#include <QLabel>
#include <QtMath>
#include <QTimeEdit>
#include <QTime>

#include "settings.h"
#include "edit_prog_dlg.h"

#define SPIN_ARROW_W 75
#define SPIN_ARROW_H 25
#define SPIN_FONT_SZ 20

EditProgDlg::EditProgDlg(struct Program &p, QWidget *parent, Qt::WindowFlags f) : QWidget(parent, f)
{
	QString sheet;
	QString text;

	m_prog = &p;

	// ************* Temperature ******************
	QLabel *temperature_label = new QLabel(QString(tr("target:")));

	set_font(temperature_label);

	p.temperature = qFloor(p.temperature * 2) / 2.0;

	m_temperature.setDecimals(1);
	m_temperature.setValue(p.temperature);
	m_temperature.setSingleStep(0.5);
	m_temperature.setSuffix(" °C");

	set_font(&m_temperature);

	/*
	 * TODO: setting size in pixels sucks
	 * We should set size in mm or ratio of the window size
	 */
	sheet = QString("QDoubleSpinBox { height: %1px;  font-size: %4px }"
		"QDoubleSpinBox::up-button { width: %2px; height: %3px }"
		"QDoubleSpinBox::down-button { width: %2px; height: %3px }")
		.arg(2*SPIN_ARROW_H)
		.arg(SPIN_ARROW_W)
		.arg(SPIN_ARROW_H)
		.arg(SPIN_FONT_SZ);
	m_temperature.setStyleSheet(sheet);

	QHBoxLayout *temperatureLayout = new QHBoxLayout;
	temperatureLayout->addWidget(temperature_label);
	temperatureLayout->addWidget(&m_temperature);

	QPushButton *ok_btn = new QPushButton(tr("Ok"), this);
	QPushButton *cancel_btn = new QPushButton(tr("Cancel"), this);


	QSizePolicy *szPolicy = new QSizePolicy(QSizePolicy::Ignored,
						QSizePolicy::MinimumExpanding,
						QSizePolicy::PushButton);

	// ************* Day of week buttons ******************
	QHBoxLayout *dowLayout = new QHBoxLayout;
	m_dow_btn[0] = new QPushButton(tr("Mo"), this);
	m_dow_btn[1] = new QPushButton(tr("Tu"), this);
	m_dow_btn[2] = new QPushButton(tr("We"), this);
	m_dow_btn[3] = new QPushButton(tr("Th"), this);
	m_dow_btn[4] = new QPushButton(tr("Fr"), this);
	m_dow_btn[5] = new QPushButton(tr("Sa"), this);
	m_dow_btn[6] = new QPushButton(tr("Su"), this);
	for (int i = 0; i < NB_DoW; i++) {
		m_dow_btn[i]->setProperty("idx", i);
		set_font(m_dow_btn[i]);
		m_dow_btn[i]->setSizePolicy(*szPolicy);
		m_dow_btn[i]->setCheckable(true);
		if (p.DoW & (1 << i))
			m_dow_btn[i]->setChecked(true);
		dowLayout->addWidget(m_dow_btn[i], 0, Qt::AlignHCenter);
	}

	// ************* start time ******************
	QHBoxLayout *startTimeLayout = new QHBoxLayout;
	QLabel *start_time_label = new QLabel(QString(tr("Start time:")));

	set_font(start_time_label);
	m_start_time_widget = new QTimeEdit(p.start_time);

	sheet = QString("QTimeEdit { height: %1px;  font-size: %4px }"
		"QTimeEdit::up-button { width: %2px; height: %3px }"
		"QTimeEdit::down-button { width: %2px; height: %3px }")
		.arg(2*SPIN_ARROW_H)
		.arg(SPIN_ARROW_W)
		.arg(SPIN_ARROW_H)
		.arg(SPIN_FONT_SZ);
	m_start_time_widget->setStyleSheet(sheet);
	startTimeLayout->addWidget(start_time_label);
	startTimeLayout->addWidget(m_start_time_widget);

	// ************* end time ******************
	QHBoxLayout *endTimeLayout = new QHBoxLayout;
	QLabel *end_time_label = new QLabel(QString(tr("End time:")));

	set_font(end_time_label);
	m_end_time_widget = new QTimeEdit(p.end_time);

	sheet = QString("QTimeEdit { height: %1px;  font-size: %4px }"
		"QTimeEdit::up-button { width: %2px; height: %3px }"
		"QTimeEdit::down-button { width: %2px; height: %3px }")
		.arg(2*SPIN_ARROW_H)
		.arg(SPIN_ARROW_W)
		.arg(SPIN_ARROW_H)
		.arg(SPIN_FONT_SZ);
	m_end_time_widget->setStyleSheet(sheet);
	endTimeLayout->addWidget(end_time_label);
	endTimeLayout->addWidget(m_end_time_widget);

	// ************* OK / CANCEL Buttons ******************
	set_font(ok_btn);
	set_font(cancel_btn);

	ok_btn->setSizePolicy(*szPolicy);
	cancel_btn->setSizePolicy(*szPolicy);

	connect(ok_btn, SIGNAL(clicked(void)), this, SLOT(save(void)));
	connect(cancel_btn, SIGNAL(clicked(void)), this, SLOT(reject(void)));

	QHBoxLayout *btnLayout = new QHBoxLayout;

	btnLayout->addWidget(cancel_btn);
	btnLayout->addWidget(ok_btn);

	QVBoxLayout *topLayout = new QVBoxLayout;

	topLayout->addLayout(temperatureLayout);

	topLayout->addLayout(dowLayout);

	topLayout->addLayout(startTimeLayout);

	topLayout->addLayout(endTimeLayout);

	topLayout->addLayout(btnLayout);

	this->setLayout(topLayout);
}

void EditProgDlg::set_font(QWidget *widget)
{
	QFont font = widget->font();
	font.setPointSize(12);
	font.setBold(true);
	widget->setFont(font);

	QSizePolicy *szQPolicy = new QSizePolicy(QSizePolicy::Minimum,
						 QSizePolicy::MinimumExpanding,
						 QSizePolicy::PushButton);
	widget->setSizePolicy(*szQPolicy);
}

void EditProgDlg::save(void)
{
	// update the prog
	m_prog->temperature = m_temperature.value();
	m_prog->DoW = 0;
	for (int i = 0; i < NB_DoW; i++) {
		if (m_dow_btn[i]->isChecked()) {
			m_prog->DoW |= 1 << i;
		}
	}
	m_prog->start_time = m_start_time_widget->time();
	m_prog->end_time = m_end_time_widget->time();

	emit close_edit_prog_dlg();
}

void EditProgDlg::reject(void)
{
	emit close_edit_prog_dlg();
}

EditProgDlg::~EditProgDlg()
{
}

/* vim: set tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab: */
