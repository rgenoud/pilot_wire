// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Qt mutizone MQTT thermostat
 *
 * Copyright (C) 2019 Richard Genoud
 *
 */


#include <QCoreApplication>
#include <QApplication>
#include <QInputDialog>
#include <QTranslator>

#include "mqttclient.h"
#include "mainwindow.h"

int main(int argc, char *argv[])
{
	QTranslator qtTranslator;
	QTranslator translator;

	Q_INIT_RESOURCE(thermostat);

	QApplication a(argc, argv);
	a.setApplicationVersion(VERSION);
	QCoreApplication::setOrganizationName("Sorico");
	QCoreApplication::setOrganizationDomain("sorico.fr");
	QCoreApplication::setApplicationName("Thermostat");

	/*
	 * Init l10n
	 */
	if (qtTranslator.load("qt_" + QLocale::system().name(),
			  QLibraryInfo::location(QLibraryInfo::TranslationsPath)))
		a.installTranslator(&qtTranslator);

	if (translator.load(":/lang/thermostat_" + QLocale::system().name() + ".qm"))
		a.installTranslator(&translator);

	MainWindow w;
	w.showFullScreen();

	return a.exec();
}

/* vim: set tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab: */
