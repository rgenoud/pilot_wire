// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Qt mutizone MQTT thermostat
 *
 * Copyright (C) 2019 Richard Genoud
 *
 */

#include <QLocale>
#include <QLabel>
#include <QDateTime>
#include <QSizePolicy>
#include <QTime>

#include "settings.h"
#include "zoneitem.h"

ZoneItem::ZoneItem(const QString &zoneName, QWidget *parent, Qt::WindowFlags f) :
	QLabel(parent, f)
{
	m_name = zoneName;
	m_heating_on = false;

	QFont font = this->font();
	font.setPointSize(12);
	this->setFont(font);

	this->setText(zoneName);

	m_temperature_value = FORCE_OFF;
	m_hygro_value = FORCE_OFF;
	m_target_temperature = FORCE_OFF;
	m_available = false;

	m_boost.temperature = 0;
	m_boost.end_date = QDateTime();

	QSizePolicy *szPolicy = new QSizePolicy(QSizePolicy::Minimum,
						QSizePolicy::MinimumExpanding,
						QSizePolicy::PushButton);

	this->setSizePolicy(*szPolicy);
}

ZoneItem::~ZoneItem()
{
}

void ZoneItem::mousePressEvent(QMouseEvent* event)
{
	Q_UNUSED(event);

	m_click_chrono.start();
}

void ZoneItem::mouseReleaseEvent(QMouseEvent* event)
{
	Q_UNUSED(event);

	if (m_click_chrono.elapsed() > 1000) {
		emit long_clicked();
	} else {
		emit clicked();
	}
}

void ZoneItem::refresh(void)
{
	QString text;
	Settings *s = Settings::getInstance();

	switch (s->getPowerState()) {
	case OFF:
		m_target_temperature = FORCE_OFF;
		break;
	case ON:
		m_target_temperature = FORCE_ON;
		break;
	case AUTO:
	case MAX_POWER_STATES:
		break;
	}

	if (s->getEndHoliday() > QDateTime::currentDateTime()) {
		m_heating_on = false;
		m_target_temperature = FORCE_OFF;
	}

	/* Zone name */
	text += QString("<center><b>") + m_name + QString("</b></center>");

	/* Temperature */
	text += QString("<center><b>");
	if (((s->getPowerState() == AUTO) && m_heating_on) || (s->getPowerState() == FORCE_ON)) {
		text += QString("<font color='red'>");
	} else {
		text += QString("<font color='black'>");
	}
	if ((m_temperature_value == FORCE_OFF) || !m_available)
		text += QString("-");
	else
		text += QString::number(m_temperature_value);
	text += QString("°C").toHtmlEscaped();
	text += QString("</font>");

	text += QString(" / ").toHtmlEscaped();
	if (m_target_temperature == FORCE_OFF) {
		text += QString("-");
	} else if (m_target_temperature == FORCE_ON) {
		text += QString("+");
	} else {
		text += QString::number(m_target_temperature);
		text += QString("°C").toHtmlEscaped();
	}
	text += QString("</b></center>");

	/* hygrometry */
	text += QString("<center><b>");
	if ((m_hygro_value == FORCE_OFF) || !m_available)
		text += QString("-");
	else
		text += QString::number(m_hygro_value);
	text += QString("%h").toHtmlEscaped();
	text += QString("</b></center><br/>");

	this->setText(text);
}

/* vim: set tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab: */
