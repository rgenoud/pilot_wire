// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Qt mutizone MQTT thermostat
 *
 * Copyright (C) 2020 Richard Genoud
 *
 */

#include <QApplication>
#include <QPushButton>
#include <QMessageBox>
#include <QGridLayout>
#include <QLocale>

#include "settings.h"
#include "menu_dlg.h"

MenuDlg::MenuDlg(QWidget *parent, Qt::WindowFlags f) :
	QWidget(parent, f)
{
	QSizePolicy *szPolicy = new QSizePolicy(QSizePolicy::Minimum,
						QSizePolicy::MinimumExpanding,
						QSizePolicy::PushButton);

	QGridLayout *mainLayout = new QGridLayout;

	QPushButton *holiday_btn = new QPushButton(tr("Holidays"), this);
	QPushButton *exit_btn = new QPushButton(tr("Exit Application"), this);
	QPushButton *back_btn = new QPushButton(tr("Back"), this);

	mainLayout->addWidget(holiday_btn, 0, 0);
	mainLayout->addWidget(exit_btn, 1, 0);
	mainLayout->addWidget(back_btn, 1, 1);

	connect(holiday_btn, SIGNAL(clicked(void)), this, SIGNAL(show_holiday_dlg(void)));
	connect(back_btn, SIGNAL(clicked(void)), this, SIGNAL(closed(void)));
	connect(exit_btn, SIGNAL(clicked(void)), this, SLOT(show_confirm_exit_dlg(void)));

	holiday_btn->setSizePolicy(*szPolicy);
	exit_btn->setSizePolicy(*szPolicy);
	back_btn->setSizePolicy(*szPolicy);

	QFont font = holiday_btn->font();
	font.setPointSize(12);
	font.setBold(true);
	holiday_btn->setFont(font);
	exit_btn->setFont(font);
	back_btn->setFont(font);

	this->setLayout(mainLayout);
}

void MenuDlg::show_confirm_exit_dlg(void)
{
	QMessageBox::StandardButton retval;

	retval = QMessageBox::question(this, tr("Exit application"), tr("Are you sure ?"));
	if (retval == QMessageBox::Yes)
		qApp->quit();
}

MenuDlg::~MenuDlg()
{
}

/* vim: set tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab: */
