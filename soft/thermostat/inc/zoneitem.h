// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Qt mutizone MQTT thermostat
 *
 * Copyright (C) 2019 Richard Genoud
 *
 */

#ifndef ZONEITEM_H
#define ZONEITEM_H

#include <QObject>
#include <QLabel>
#include <QElapsedTimer>

#include "boost_dlg.h"

class ZoneItem : public QLabel
{
	Q_OBJECT

public:
	explicit ZoneItem(const QString &zoneName, QWidget *parent = Q_NULLPTR,
			  Qt::WindowFlags f = Qt::WindowFlags());
	~ZoneItem();
	void refresh(void);

	double m_temperature_value;
	double m_hygro_value;
	double m_target_temperature;
	bool m_available;
	bool m_heating_on;
	QString m_name;
	struct boost_data m_boost;

signals:
	void clicked();
	void long_clicked();

protected:
	void mousePressEvent(QMouseEvent* event);
	void mouseReleaseEvent(QMouseEvent* event);
	QElapsedTimer m_click_chrono;
};

#endif // ZONEITEM_H

/* vim: set tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab: */
