// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Qt mutizone MQTT thermostat
 *
 * Copyright (C) 2020 Richard Genoud
 *
 */

#ifndef EDITPROGDLG_H
#define EDITPROGDLG_H

#include <QWidget>
#include <QObject>
#include <QPushButton>
#include <QTimeEdit>
#include <QDoubleSpinBox>

#include "settings.h"

#define NB_DoW 7

class EditProgDlg : public QWidget
{
	Q_OBJECT

public:
	EditProgDlg(struct Program &p, QWidget *parent = Q_NULLPTR,
		    Qt::WindowFlags f = Qt::WindowFlags());
	~EditProgDlg();

private:
	void set_font(QWidget *widget);
	QDoubleSpinBox m_temperature;
	QPushButton *m_dow_btn[NB_DoW];
	QTimeEdit *m_start_time_widget;
	QTimeEdit *m_end_time_widget;
	struct Program *m_prog;

private slots:
	void save(void);
	void reject(void);

signals:
	void close_edit_prog_dlg(void);
};

#endif // EDITPROGDLG_H

/* vim: set tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab: */
