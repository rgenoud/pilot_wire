// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Qt mutizone MQTT thermostat
 *
 * Copyright (C) 2019 Richard Genoud
 *
 */

#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>
#include <QDateTime>
#include <QVector>
#include <QTime>

#define MAX_NB_ZONES 4
#define FORCE_OFF (-256)
#define FORCE_ON (99)

enum power_states {
	OFF,
	ON,
	AUTO,
	MAX_POWER_STATES
};

struct Heater {
	QString ctrl_topic;
};

struct Program {
	double temperature;
	uint8_t DoW;
	QTime start_time;
	QTime end_time;
};

struct Room {
	QString name;
	QString sensor_topic;
	QString availability_topic;
	double default_temperature;
	QVector<struct Heater> heaters;
	QVector<struct Program> progs;
};

struct Brocker {
	QString address;
	int port;
};

class Settings : public QSettings
{
	Q_OBJECT
public:
	static Settings *getInstance(void);
	int nbZones(void);
	enum power_states getPowerState(void) { return m_state; }
	void setPowerState(enum power_states state);
	const QDateTime &getEndHoliday(void) { return m_end_holiday; }
	void setEndHoliday(const QDateTime &endDate);
	const struct Room &getRoom(int idx) { return m_rooms.at(idx); }
	void setRoomDefaultTemperature(int idx, double temperature);
	void setRoomPrograms(int idx, QVector<struct Program> &progs);

	struct Brocker m_broker;

private:
	explicit Settings(QObject *parent = Q_NULLPTR);
	~Settings(void);
	static Settings *_singleton;

	void set_rooms_default_config(void);
	int read_config(void);

	enum power_states m_state;
	QDateTime m_end_holiday;
	QVector<struct Room> m_rooms;
};

#endif // SETTINGS_H

/* vim: set tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab: */

