// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Qt mutizone MQTT thermostat
 *
 * Copyright (C) 2019 Richard Genoud
 *
 */

#ifndef MQTTCLIENT_H
#define MQTTCLIENT_H

#include <QTimer>

#include <QtMqtt/QtMqtt>

class MQTTClient: public QMqttClient
{
	Q_OBJECT
public:
	explicit MQTTClient(const QString& host = "localhost", int port = 1883,
			    QObject* parent = NULL);
	virtual ~MQTTClient();
	void publish_msg(const QString& topic, const QString& payload);

private:
	QTimer m_timer;

public slots:
	void onConnected();
	void onReceived(const QByteArray &message, const QMqttTopicName &topic);
	void onError(ClientError error);
	void onPublished(qint32 msgid);
	void onStateChanged(ClientState state);
	void onDisconnected(void);
	void allHeatersOn(bool on);
	void onConnectTimeout(void);

signals:
	void new_temperature(int idx, double val);
	void new_hygro(int idx, double val);
	void new_battery(int idx, double val);
	void new_availability(int idx, bool ok);
};

#endif

/* vim: set tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab: */
