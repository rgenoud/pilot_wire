// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Qt mutizone MQTT thermostat
 *
 * Copyright (C) 2020 Richard Genoud
 *
 */

#ifndef EDITDLG_H
#define EDITDLG_H

#include <QVector>
#include <QWidget>
#include <QObject>
#include <QSpinBox>
#include <QDateTime>
#include <QVBoxLayout>
#include <QPushButton>
#include <QDoubleSpinBox>

#include "settings.h"

class EditDlg : public QWidget
{
	Q_OBJECT

public:
	EditDlg(int idx, QWidget *parent = Q_NULLPTR,
		Qt::WindowFlags f = Qt::WindowFlags());
	~EditDlg();
	void refresh_progs(void);

private:
	void set_font(QWidget *widget);
	QHBoxLayout *add_prog_layout(int idx);

	QPushButton *m_add_btn;
	QDoubleSpinBox m_default_temperature;
	QVBoxLayout *m_progsLayout;
	QVector<struct Program> m_progs;
	int m_idx; // room index

private slots:
	void save(void);
	void add(void);
	void reject(void);
	void delete_prog_clicked(void);
	void edit_prog_clicked(void);

signals:
	void close_edit_dlg(void);
	void show_edit_prog_dlg(struct Program &p);
};

#endif // EDITDLG_H

/* vim: set tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab: */
