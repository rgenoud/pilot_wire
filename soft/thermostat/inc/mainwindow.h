// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Qt mutizone MQTT thermostat
 *
 * Copyright (C) 2019 Richard Genoud
 *
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QStackedWidget>
#include <QMainWindow>
#include <QPushButton>
#include <QtWidgets>
#include <QVector>
#include <QTimer>
#include <QDate>

#include "mqttclient.h"
#include "zoneitem.h"
#include "boost_dlg.h"
#include "menu_dlg.h"
#include "edit_dlg.h"
#include "edit_prog_dlg.h"
#include "settings.h"

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = NULL);
	~MainWindow();

private:
	MQTTClient *m_mqtt;
	QPushButton *m_state_btn;
	QVector<ZoneItem *> m_zones;
	QTimer *m_timer;
	BoostDlg *m_boost;
	EditDlg *m_edit_dlg;
	MenuDlg *m_menu;
	QLabel *m_holiday_label;
	QStackedWidget m_central_widget;
	void update_state_btn(enum power_states st);
	double get_target_temperature(int room_idx);
	bool get_heater_order(int room_idx);
	void apply_automatic_state(void);

private slots:
	void temperature_slot(int idx, double val);
	void hygro_slot(int idx, double val);
	void availability_slot(int idx, bool ok);
	void change_state(void);
	void apply_order_to_heaters(void);
	void show_boost(void);
	void do_show_edit_dlg(void);
	void show_main(void);
	void show_menu(void);
	void boost_slot(int idx, struct boost_data &data);
	void do_show_holiday_dlg(void);
	void do_close_holiday_dlg(void);
	void do_show_edit_prog_dlg(struct Program &p);
	void do_close_edit_prog_dlg(void);
	void set_holiday_mode(QDateTime end_date);

signals:
	void setAllHeatersOn(bool on);

};

#endif // MAINWINDOW_H

/* vim: set tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab: */
