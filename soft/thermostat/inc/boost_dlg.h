// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Qt mutizone MQTT thermostat
 *
 * Copyright (C) 2020 Richard Genoud
 *
 */

#ifndef BOOSTDLG_H
#define BOOSTDLG_H

#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QWidget>
#include <QObject>
#include <QDateTime>

struct boost_data
{
	double temperature;
	QDateTime end_date;
};

class BoostDlg : public QWidget
{
	Q_OBJECT

public:
	BoostDlg(int idx, const QString &zoneName,
		 double actual_temperature,
		 double target_temperature = 21,
		 QWidget *parent = Q_NULLPTR,
		 Qt::WindowFlags f = Qt::WindowFlags());
	~BoostDlg();
	int m_idx; // room index

private:
	QDoubleSpinBox m_boost_temperature;
	QSpinBox m_boost_duration;

private slots:
	void send_result(void);
	void reject(void);
	void updateDuration(int val);

signals:
	void boost_changed(int idx, struct boost_data &data);
};

#endif // BOOSTDLG_H

/* vim: set tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab: */
