// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Qt mutizone MQTT thermostat
 *
 * Copyright (C) 2020 Richard Genoud
 *
 */

#ifndef HOLIDAYDLG_H
#define HOLIDAYDLG_H

#include <QWidget>
#include <QObject>
#include <QSpinBox>
#include <QDateTime>
#include <QCalendarWidget>

class HolidayDlg : public QWidget
{
	Q_OBJECT

public:
	HolidayDlg(QWidget *parent = Q_NULLPTR,
		 Qt::WindowFlags f = Qt::WindowFlags());
	~HolidayDlg();

private:
	QCalendarWidget m_cal;
	QSpinBox m_time;

signals:
	void close_holiday_dlg(void);
	void holiday_mode(QDateTime end_date);

private slots:
	void validate_holiday_mode(void);

};

#endif // HOLIDAYDLG_H

/* vim: set tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab: */

