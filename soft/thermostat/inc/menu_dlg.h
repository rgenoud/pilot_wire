// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Qt mutizone MQTT thermostat
 *
 * Copyright (C) 2020 Richard Genoud
 *
 */

#ifndef MENUDLG_H
#define MENUDLG_H

#include <QWidget>
#include <QObject>
#include <QPushButton>

class MenuDlg : public QWidget
{
	Q_OBJECT

public:
	MenuDlg(QWidget *parent = Q_NULLPTR,
		 Qt::WindowFlags f = Qt::WindowFlags());
	~MenuDlg();

signals:
	void exit_requested(void);
	void closed(void);
	void show_holiday_dlg(void);
	void show_save_cfg_dlg(void);
	void show_load_cfg_dlg(void);

private slots:
	void show_confirm_exit_dlg(void);
};

#endif // MENUDLG_H

/* vim: set tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab: */
